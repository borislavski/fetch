const DOMAIN = 'https://swapi.dev/api/films/';
const list = document.createElement('dl');
const loadItem = document.querySelector('.movieWrapper');
list.classList.add('movie-list')
document.body.appendChild(list);

getMovies();

function getMovies(){
    fetch(DOMAIN)
    .then(response => response.json())
    .then(data => {
        data.results.forEach(({
            title, characters, episode_id, opening_crawl
        }) => {
            const movieList = document.createElement('dt');
            const episodeList = document.createElement('dd'); 
            const description = document.createElement('dd'); 
            movieList.innerText = title;
            movieList.style.marginBottom = '20px'
            movieList.style.color = 'dark-grey'
            episodeList.innerHTML = `<span>Episode:</span> ${episode_id}`;
            episodeList.style.color = 'tomato'
            description.innerHTML = `<span>Description:</span><br> ${opening_crawl}`;
            description.style.fontStyle = 'italic'
            list.appendChild(movieList);
            movieList.appendChild(episodeList);
            movieList.appendChild(description);
            
            getCharacters (characters, episodeList); 
        });
    });
}

function getCharacters (characters, movieList){
    const charactersList = document.createElement('dd'); 
    charactersList.innerHTML = '<span>Characters in the movie:</span><br>';
    movieList.before(charactersList);
    goLoad (charactersList);
    const charactersName = []; 
    
    characters.forEach((item) => {
        fetch(item)
        .then(response => response.json())
        .then(data => { 
            const listOfCharacters = document.createElement('li'); 
            listOfCharacters.innerText = data.name;
            charactersName.push(listOfCharacters);
            if(characters.length === charactersName.length) {
                charactersName.forEach((item) => {
                    hideLoad (charactersList);
                    charactersList.appendChild(item);
                });
            }
        });
    });
}

function goLoad (holder) {
    const loadOn = loadItem.cloneNode(true);
    loadOn.style.display = 'inline-block';
    holder.appendChild(loadOn);
}

function hideLoad (charactersList) {
    const loader = charactersList.querySelector('.movieWrapper');
    loader.style.display = 'none';
}